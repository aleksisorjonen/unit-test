
const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

// Endpoint localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!')
})

// Endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a)
    const b = parseInt(req.query.b)
    console.log({a , b})
    const total = mylib.sum(a,b)
    res.send('Add works! ' + total.toString())
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
